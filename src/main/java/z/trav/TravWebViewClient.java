package z.trav;

import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by Z on 2015-01-31.
 */
public class TravWebViewClient extends WebViewClient {

    private static final String MAIN_VILLAGE_URL = "http://ts2.travian.pl/dorf1.php?newdid=2651&";
    private static final String RAID_LIST_URL = "http://ts2.travian.pl/build.php?tt=99&id=39";
    private int raidListCount;
    private int numberOfRaidsSent;

    public TravWebViewClient(int raidListCount) {
        this.raidListCount = raidListCount;
        numberOfRaidsSent = 0;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        Log.e("SERV", "onPageFinished wywołane dla adresu: " + url);
        if (url.equals(MAIN_VILLAGE_URL)) {
            checkForLogin(view);
            checkForAttack(view);
        }
        if (url.equals(RAID_LIST_URL)) {
            startRaid(view);
        }
    }

    private void startRaid(WebView view) {
        selectCheckboxes(view);
        int i = numberOfRaidsSent % raidListCount;
        Log.e("SERV", "wysyłanie listy " + i);
        clickRaidButton(view, i);
        numberOfRaidsSent++;
    }

    private void checkForAttack(WebView view) {
        view.loadUrl("javascript:window.HTMLOUT.getAttacks(document.getElementsByClassName(' attack').length);");
    }

    private void checkForLogin(WebView view) {
        view.loadUrl("javascript:window.HTMLOUT.checkLoginButton(document.getElementById('s1'));");
    }

    private void selectCheckboxes(WebView view) {
        view.loadUrl("javascript:(function() { " +
                "var rows = document.getElementsByClassName('slotRow');" +
                "for(var i = 0; i < rows.length; i++) {" +
                    "var checkbox = rows[i].getElementsByTagName('input')[0];" +
                    "if (checkbox != null) {" +
                        "if(rows[i].getElementsByClassName('iReport iReport1').length == 1) {" +
                            "checkbox.checked = true;" +
                        "} else {" +
                            "checkbox.checked = false;" +
                        "}" +
                    "}" +
                "}" +
                "})()");
    }

    private void clickRaidButton(WebView view, int i) {
        view.loadUrl("javascript:(function() { " +
                "var buttons = document.getElementsByTagName('button');" +
                "var raidButtons = [];" +
                "for (var i = 0; i < buttons.length; i++) {" +
                    "var button = buttons[i];" +
                    "if (button.getAttribute('value') == 'wyślij na grabież') {" +
                        "var id = button.getAttribute('id');" +
                        "raidButtons[raidButtons.length] = button;" +
                    "}" +
                "}" +
                "e=document.createEvent('HTMLEvents');" +
                "e.initEvent('click',true,true);raidButtons["+i+"].dispatchEvent(e);" +
                "})()");
    }
}
