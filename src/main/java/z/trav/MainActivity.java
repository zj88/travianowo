package z.trav;

import android.app.*;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {

    private static final String STOP_FARMING = "Stop farming";
    private static final String START_FARMING = "Start farming";
    private static final String MUTE_ALARM = "Mute Alarm";
    private static final String UNMUTE_ALARM = "Unmute Alarm";
    private static final String FARMING_IS_ON = "Farming is On";
    private static final String TS2_TRAVIAN_PL = "http://ts2.travian.pl";
    private static final String NOTIFICATION_TEXT = "Ts2 farming is On";
    private static final String FARMING_IS_OFF = "Farming is Off";
    private static final String NOTIFICATION_TITLE = "Travianowo";
    private android.webkit.WebView webView;
    private boolean isBound;
    private boolean serviceIsRepeating;
    private Button ignoreButton;
    private NotificationManager notificationManager;
    private int NOTIFICATION = R.string.action_service;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        webView = (WebView) findViewById(R.id.webView);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setDisplayZoomControls(false);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(TS2_TRAVIAN_PL);

        final AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        final Intent periodicalIntent = new Intent();
        periodicalIntent.setClass(getApplicationContext(), PeriodicalService.class);

        final Button repeatingServiceButton = (Button) findViewById(R.id.startButton);

        serviceIsRepeating = (PendingIntent.getService(getApplicationContext(), 1111, periodicalIntent, PendingIntent.FLAG_NO_CREATE) != null);
        if (serviceIsRepeating) {
            Log.e("MAIN_ACT", "found repeating alarm on");
            Toast.makeText(getBaseContext(), FARMING_IS_ON, Toast.LENGTH_SHORT).show();
            repeatingServiceButton.setText(STOP_FARMING);
        } else {
            repeatingServiceButton.setText(START_FARMING);
        }
        final PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 1111, periodicalIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        repeatingServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!serviceIsRepeating) {
                    startService(periodicalIntent);
                    serviceIsRepeating = true;
                    repeatingServiceButton.setText(STOP_FARMING);
                    Toast.makeText(getBaseContext(), FARMING_IS_ON, Toast.LENGTH_SHORT).show();
                    showNotification();
                } else {
                    alarmManager.cancel(pendingIntent);
                    Log.e("MAIN_ACT", "canceling periodical service");
                    serviceIsRepeating = false;
                    repeatingServiceButton.setText(START_FARMING);
                    Toast.makeText(getBaseContext(), FARMING_IS_OFF, Toast.LENGTH_SHORT).show();
                    notificationManager.cancel(NOTIFICATION);
                }
            }
        });

        ignoreButton = (Button) findViewById(R.id.ignoreAlarmButton);
        ignoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doBindService();
                if (ignoreButton.getText().equals(MUTE_ALARM)) {
                    ignoreButton.setText(UNMUTE_ALARM);
                } else {
                    ignoreButton.setText(MUTE_ALARM);
                }
                ignoreButton.setEnabled(false);
            }
        });
    }

    private PeriodicalService boundService;

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            boundService = ((PeriodicalService.ServiceBinder)service).getService();
            isBound = true;
            boundService.stopAlarm();
            boundService.ignoreAlarmTrigger();
            ignoreButton.setEnabled(true);
            doUnbindService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            boundService = null;
            isBound = false;
        }
    };

    void doBindService() {
        bindService(new Intent(this, PeriodicalService.class), connection, Context.BIND_AUTO_CREATE);
    }
    void doUnbindService() {
        if (isBound) {
            unbindService(connection);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (webView.canGoBack()) {
                webView.goBack();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void showNotification() {
        Notification notification = new Notification(R.drawable.t_icon, NOTIFICATION_TEXT, System.currentTimeMillis());
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);
        notification.setLatestEventInfo(this, NOTIFICATION_TITLE, NOTIFICATION_TEXT, contentIntent);
        notificationManager.notify(NOTIFICATION, notification);
    }
}

