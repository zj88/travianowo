package z.trav;

import android.app.*;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.*;
import android.util.Log;
import android.view.Gravity;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.Random;

/**
 * Created by Z on 2015-02-18.
 */
public class PeriodicalService extends Service{

    private static final int RAID_INTERVAL_MIN = 25;
    private static final int RAID_INTERVAL_MAX = 35;
    private static final int WINDOW = 5;
    private static final int RAID_LIST_DELAY = 8;
    private static final String WAKE_TAG = "WakeTag";
    private static final String JS_INTERFACE = "HTMLOUT";
    //private static final String MAIN_VILLAGE_URL = "http://ts2.travian.pl/dorf1.php";
    private static final String MAIN_VILLAGE_URL = "http://ts2.travian.pl/dorf1.php?newdid=2651&";
    private static final String SECONDARY_VILLAGE_URL = "http://ts2.travian.pl/dorf1.php?newdid=73104&";
    private static final String RAID_LIST_URL = "http://ts2.travian.pl/build.php?tt=99&id=39";
    private int raidListCount = 2;
    private PowerManager.WakeLock wakeLock;
    private JSInterface jsInterface;
    private WebView invisibleWebView;
    private LinearLayout webViewWindow;

    @Override
    public void onDestroy() {
        Log.e("SERV", "Destroying service ...");
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e("SERV", "Received start id " + startId + ": " + intent);
        startFarming(invisibleWebView, webViewWindow);
        setNextAlarm();
        stopSelf();
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e("SERV", "Creating service ...");
        createWebViewWindow();

    }

    private void createWebViewWindow() {
        Log.e("SERV", "Creating WebViewWindow ...");

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, WAKE_TAG);

        WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.TYPE_PHONE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM, PixelFormat.TRANSLUCENT);
        params.gravity = Gravity.TOP | Gravity.LEFT;
        params.x = 0;
        params.y = 0;
        params.width = 30;
        params.height = 80;

        webViewWindow = new LinearLayout(this);
        webViewWindow.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));

        invisibleWebView = new WebView(this);
        WebSettings webSettings = invisibleWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        jsInterface = new JSInterface(getBaseContext());
        invisibleWebView.addJavascriptInterface(jsInterface, JS_INTERFACE);
        invisibleWebView.setWebViewClient(new TravWebViewClient(raidListCount));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        Log.e("SERV", "invisibleWebView settings up");
        invisibleWebView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        webViewWindow.addView(invisibleWebView);
        invisibleWebView.loadUrl(MAIN_VILLAGE_URL);
        Log.e("SERV", "url loading...");

        windowManager.addView(webViewWindow, params);
    }

    protected void setNextAlarm() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent periodicalIntent = new Intent();
        periodicalIntent.setClass(getApplicationContext(), PeriodicalService.class);
        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 1111, periodicalIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Random random = new Random();
        int interval = random.nextInt(RAID_INTERVAL_MAX - RAID_INTERVAL_MIN + 1) + RAID_INTERVAL_MIN;
        Log.e("SERV", "next alarm in " + interval + " +- " + WINDOW);
        alarmManager.setWindow(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + (interval - WINDOW) * 60 * 1000, 2 * WINDOW * 60 * 1000, pendingIntent);
    }

    private void startFarming(final WebView invisibleWebView, final LinearLayout webViewWindow) {
        final Handler handler = new Handler();
        final Runnable sendFarmRunnable = new Runnable() {
            @Override
            public void run() {
                invisibleWebView.loadUrl(RAID_LIST_URL);
            }
        };
        final Runnable changeToSecondaryVillage = new Runnable() {
            @Override
            public void run() {
                invisibleWebView.loadUrl(SECONDARY_VILLAGE_URL);
            }
        };
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    wakeLock.acquire();
                    sleep(3 * 1000);
                    for (int i = 0; i < raidListCount; i++){
                        handler.post(sendFarmRunnable);
                        sleep(RAID_LIST_DELAY * 1000);
                    }
                    handler.post(changeToSecondaryVillage);
                    WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
                    windowManager.removeView(webViewWindow);
                    wakeLock.release();
                } catch (InterruptedException e) {
                    Log.e("SERV", "erroring :(");
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }

    public void stopAlarm() {
        jsInterface.stopAlarm();
    }

    public void ignoreAlarmTrigger() {
        jsInterface.ignoreAlarmTrigger();
        WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        windowManager.removeView(webViewWindow);
    }

    public class ServiceBinder extends Binder {
        PeriodicalService getService() {
            return PeriodicalService.this;
        }
    }

    private final IBinder serviceBinder = new ServiceBinder();

    @Override
    public IBinder onBind(Intent intent) {
        Log.e("SERV", "binding to service");
        return serviceBinder;
    }
}