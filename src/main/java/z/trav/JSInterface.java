package z.trav;

import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

/**
 * Created by Z on 2015-02-03.
 */
public class JSInterface {

    private static final CharSequence LOGGED_OUT = "Travianowo logged out!";
    private final Context context;
    private static boolean alarmStarted;
    private static Ringtone r;
    private static boolean alarmIgnored;

    public JSInterface(Context context) {
        this.context = context;
    }

    @JavascriptInterface
    public void getAttacks(String attacksNumber) {
        Log.e("JS INT", "checking for attacks");
        int aNum = Integer.parseInt(attacksNumber);
        if (aNum != 0) {
            if (!alarmStarted && !alarmIgnored) {
                Log.e("JS INT", "ALARM !!!");
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                r = RingtoneManager.getRingtone(context, notification);
                r.play();
                alarmStarted = true;
            }
        }
    }

    @JavascriptInterface
    public void checkLoginButton(Object button) {
        if (button != null) {
            Log.e("JS INT", "Logged out!!");
            Toast.makeText(context, LOGGED_OUT, Toast.LENGTH_LONG).show();
        }
    }

    public void stopAlarm() {
        if (alarmStarted) {
            r.stop();
            alarmStarted = false;
        }
    }

    public void ignoreAlarmTrigger() {
        if (!alarmIgnored)
            alarmIgnored = true;
        else
            alarmIgnored = false;
    }
}
